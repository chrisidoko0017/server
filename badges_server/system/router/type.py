from uuid import uuid4

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy import delete, select
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_202_ACCEPTED,
    HTTP_403_FORBIDDEN,
    HTTP_404_NOT_FOUND,
    HTTP_409_CONFLICT,
)

from badges_server.config import logrdata
from badges_server.database.objs import Type, User
from badges_server.system.auth import dep_user
from badges_server.system.database import dep_db_async_session
from badges_server.system.models.type import (
    TypeCreateModel,
    TypeModelExternal,
    TypeResult,
    TypeUpdateModel,
)

router = APIRouter(prefix="/types")


@router.get("/name/{name}", status_code=HTTP_200_OK, response_model=TypeResult, tags=["types"])
async def select_type_by_name(
    name: str, db_async_session: AsyncSession = Depends(dep_db_async_session)
):
    """
    Return the type with the specified name
    """
    query = select(Type).filter_by(name=name).options(selectinload("*"))
    result = await db_async_session.execute(query)
    type_data = result.scalar_one_or_none()
    if not type_data:
        raise HTTPException(
            HTTP_404_NOT_FOUND, f"Type with the requested name '{name}' was not found"
        )
    return {"action": "get", "type": type_data}


@router.get("/{uuid}", status_code=HTTP_200_OK, response_model=TypeResult, tags=["types"])
async def select_user(uuid: str, db_async_session: AsyncSession = Depends(dep_db_async_session)):
    """
    Return the type with the specified UUID
    """
    query = select(Type).filter_by(uuid=uuid).options(selectinload("*"))
    result = await db_async_session.execute(query)
    type_data = result.scalar_one_or_none()
    if not type_data:
        raise HTTPException(
            HTTP_404_NOT_FOUND, f"Type with the requested UUID '{uuid}' was not found"
        )
    return {"action": "get", "type": type_data}


@router.post("", status_code=HTTP_201_CREATED, response_model=TypeResult, tags=["types"])
async def create_type(
    data: TypeCreateModel,
    db_async_session: AsyncSession = Depends(dep_db_async_session),
    user: User = Depends(dep_user),
):
    """
    Create a type with the requested attributes
    """
    if not user.headuser:
        raise HTTPException(
            HTTP_403_FORBIDDEN,
            "Access to this endpoint is now allowed for users with inadequate access levels",
        )
    made_type = Type(
        name=data.name,
        desc=data.desc,
        arranged=data.arranged,
        uuid=uuid4().hex[0:8],
    )
    db_async_session.add(made_type)
    type_result = TypeModelExternal.from_orm(made_type).dict()
    try:
        await db_async_session.flush()
    except IntegrityError as expt:
        logrdata.logrobjc.warning("Uniqueness constraint failed - Please try again")
        logrdata.logrobjc.warning(str(expt))
        raise HTTPException(HTTP_409_CONFLICT, "Uniqueness constraint failed - Please try again")
    return {"action": "post", "type": type_result}


@router.patch("/{uuid}", status_code=HTTP_202_ACCEPTED, response_model=TypeResult, tags=["types"])
async def update_type(
    uuid: str,
    data: TypeUpdateModel,
    db_async_session: AsyncSession = Depends(dep_db_async_session),
    user: User = Depends(dep_user),
):
    """
    Update the records for the type with the requested UUID
    """
    if not user.headuser:
        raise HTTPException(
            HTTP_403_FORBIDDEN,
            "Access to this endpoint is now allowed for users with inadequate access levels",
        )
    query = select(Type).filter_by(uuid=uuid).options(selectinload("*"))
    result = await db_async_session.execute(query)
    type_data = result.scalar_one_or_none()
    if not type_data:
        raise HTTPException(
            HTTP_404_NOT_FOUND, f"Type with the requested UUID '{uuid}' was not found"
        )

    """
    TODO: We are trying to get a database-level check here for the validity of the field's
    information. See https://gitlab.com/fedora/websites-apps/fedora-badges/server/-/issues/39 for
    additional details
    """

    """
    An empty string (i.e. "") as a value is not acceptable in the `name` data field but if no
    value is set on that field, it defaults to `None`. As the truthy value for an empty string
    (i.e. "") is False as so is of `None` in Python programming language - but with the `name`
    field being empty not being possible - the condition `if data.name` suffices and the existing
    stored value is retained in both the cases whenever an empty string (i.e. "") is explicitly
    passed and whenever no value is set on the `desc` field.
    """

    if data.name:
        type_data.name = data.name.strip()

    """
    An empty string (i.e. "") as a value is acceptable in the `desc` data field but if no value is
    set on that field, it defaults to `None`. As the truthy value for an empty string (i.e. "") is
    False in Python programming language and it is possible for the `desc` field to be empty, we
    cannot use the `if data.desc` condition as it would always result in the resetting of the
    previously stored value in the `desc` field to an empty string whenever no value is set on the
    `desc` field, in case someone wants to only update the `name` field.
    """

    if data.desc is not None:
        type_data.desc = data.desc.strip()

    try:
        await db_async_session.flush()
    except IntegrityError as expt:
        logrdata.logrobjc.warning("Uniqueness constraint failed - Please try again")
        logrdata.logrobjc.warning(str(expt))
        raise HTTPException(HTTP_409_CONFLICT, "Uniqueness constraint failed - Please try again")
    return {"action": "put", "type": type_data}


@router.delete("/{uuid}", status_code=HTTP_202_ACCEPTED, response_model=TypeResult, tags=["types"])
async def delete_type(
    uuid: str,
    db_async_session: AsyncSession = Depends(dep_db_async_session),
    user: User = Depends(dep_user),
):
    """
    Delete the description for the type with the requested UUID
    """
    if not user.headuser:
        raise HTTPException(
            HTTP_403_FORBIDDEN,
            "Access to this endpoint is now allowed for users with inadequate access levels",
        )
    query = select(Type).filter_by(uuid=uuid).options(selectinload("*"))
    result = await db_async_session.execute(query)
    type_data = result.scalar_one_or_none()
    if not type_data:
        raise HTTPException(
            HTTP_404_NOT_FOUND, f"Type with the requested UUID '{uuid}' was not found"
        )
    query = delete(Type).filter_by(id=type_data.id)
    await db_async_session.execute(query)
    try:
        await db_async_session.flush()
    except IntegrityError as expt:
        logrdata.logrobjc.warning("Uniqueness constraint failed - Please try again")
        logrdata.logrobjc.warning(str(expt))
        raise HTTPException(HTTP_409_CONFLICT, "Uniqueness constraint failed - Please try again")
    return {"action": "delete", "type": type_data}
