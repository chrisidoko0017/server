from uuid import uuid4

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy import select
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import selectinload
from starlette.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_202_ACCEPTED,
    HTTP_403_FORBIDDEN,
    HTTP_404_NOT_FOUND,
    HTTP_409_CONFLICT,
    HTTP_422_UNPROCESSABLE_ENTITY,
)

from badges_server.config import logrdata
from badges_server.database.objs import Access, User
from badges_server.system.auth import dep_user
from badges_server.system.database import dep_db_async_session
from badges_server.system.models.user import (
    UserCreateModel,
    UserModelExternal,
    UserModelInternal,
    UserResult,
    UserUpdateModel,
)

router = APIRouter(prefix="/users")


@router.get(
    "/username/{username}", status_code=HTTP_200_OK, response_model=UserResult, tags=["users"]
)
async def select_user_by_username(
    username: str, db_async_session: AsyncSession = Depends(dep_db_async_session)
):
    """
    Return the user with the specified username
    """
    query = select(User).filter_by(username=username).options(selectinload("*"))
    result = await db_async_session.execute(query)
    user_data = result.scalar_one_or_none()
    if not user_data:
        raise HTTPException(
            HTTP_404_NOT_FOUND, f"User with the requested username '{username}' was not found"
        )
    return {"action": "get", "user": user_data}


@router.get("/{uuid}", status_code=HTTP_200_OK, response_model=UserResult, tags=["users"])
async def select_user_by_uuid(
    uuid: str, db_async_session: AsyncSession = Depends(dep_db_async_session)
):
    """
    Return the user with the specified UUID
    """
    query = select(User).filter_by(uuid=uuid).options(selectinload("*"))
    result = await db_async_session.execute(query)
    user_data = result.scalar_one_or_none()
    if not user_data:
        raise HTTPException(
            HTTP_404_NOT_FOUND, f"User with the requested UUID '{uuid}' was not found"
        )
    return {"action": "get", "user": user_data}


@router.post("", status_code=HTTP_201_CREATED, response_model=UserResult, tags=["users"])
async def create_user(
    data: UserCreateModel,
    db_async_session: AsyncSession = Depends(dep_db_async_session),
    user: User = Depends(dep_user),
):
    """
    Create a user with the requested attributes
    """
    if not user.headuser:
        raise HTTPException(
            HTTP_403_FORBIDDEN,
            "Access to this endpoint is now allowed for users with inadequate access levels",
        )
    made_user = User(
        username=data.username,
        mailaddr=data.mailaddr,
        desc=data.desc,
        withdraw=False,
        headuser=False,
        uuid=uuid4().hex[0:8],
    )
    db_async_session.add(made_user)
    try:
        await db_async_session.flush()
    except IntegrityError as expt:
        logrdata.logrobjc.warning("Uniqueness constraint failed - Please try again")
        logrdata.logrobjc.warning(str(expt))
        raise HTTPException(HTTP_409_CONFLICT, "Uniqueness constraint failed - Please try again")
    made_access = Access(
        user_id=UserModelInternal.from_orm(made_user).id,
        code=uuid4().hex,
        active=True,
        uuid=uuid4().hex[0:8],
    )
    db_async_session.add(made_access)
    user_result = UserModelExternal.from_orm(made_user).dict()
    try:
        await db_async_session.flush()
    except IntegrityError as expt:
        logrdata.logrobjc.warning("Uniqueness constraint failed - Please try again")
        logrdata.logrobjc.warning(str(expt))
        raise HTTPException(HTTP_409_CONFLICT, "Uniqueness constraint failed - Please try again")
    await db_async_session.flush()
    return {"action": "post", "user": user_result}


@router.patch("/{uuid}", status_code=HTTP_202_ACCEPTED, response_model=UserResult, tags=["users"])
async def update_user(
    uuid: str,
    data: UserUpdateModel,
    db_async_session: AsyncSession = Depends(dep_db_async_session),
    user: User = Depends(dep_user),
):
    """
    Update the records for the user with the requested UUID
    """

    if not user.headuser:
        raise HTTPException(
            HTTP_403_FORBIDDEN,
            "Access to this endpoint is now allowed for users with inadequate access levels",
        )
    query = select(User).filter_by(uuid=uuid).options(selectinload("*"))
    result = await db_async_session.execute(query)
    user_data = result.scalar_one_or_none()
    if not user_data:
        raise HTTPException(
            HTTP_404_NOT_FOUND, f"User with the requested UUID '{uuid}' was not found"
        )

    """
    TODO: We are trying to get a database-level check here for the validity of the field's
    information. See https://gitlab.com/fedora/websites-apps/fedora-badges/server/-/issues/39 for
    additional details
    """

    if user_data.withdraw is False:
        """
        A user account needs to be ACTIVE (i.e. user_data.withdraw = False) for changing the
        records like that of `headuser`, `desc` and `mailaddr`.
        """

        if data.headuser is not None:
            user_data.headuser = data.headuser

        """
        See comment at
        https://gitlab.com/fedora/websites-apps/fedora-badges/server/-/blob/main/badges_server/system/router/type.py?ref_type=heads#L135-L142
        """

        if data.desc is not None:
            user_data.desc = data.desc

        """
        See comment at
        https://gitlab.com/fedora/websites-apps/fedora-badges/server/-/blob/main/badges_server/system/router/type.py?ref_type=heads#L123-L130
        """

        if data.mailaddr:
            user_data.mailaddr = data.mailaddr

        if data.withdraw is not None:
            user_data.withdraw = data.withdraw

    elif user_data.withdraw is True and data.withdraw is not None:
        """
        A user account needs not to be ACTIVE for changing the records like that of `withdraw`.
        This can be useful for REACTIVATING a previously DEACTIVATED user account.
        """

        user_data.withdraw = data.withdraw

    else:
        raise HTTPException(
            HTTP_422_UNPROCESSABLE_ENTITY,
            f"User with the requested UUID '{uuid}' have withdrawn from the service so their access levels cannot be modified",  # noqa: E501
        )

    try:
        await db_async_session.flush()
    except IntegrityError as expt:
        logrdata.logrobjc.warning("Uniqueness constraint failed - Please try again")
        logrdata.logrobjc.warning(str(expt))
        raise HTTPException(HTTP_409_CONFLICT, "Uniqueness constraint failed - Please try again")
    return {"action": "put", "user": user_data}
